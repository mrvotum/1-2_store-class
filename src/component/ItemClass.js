import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ItemClass extends Component {
  constructor(props) {
    super(props);
    this.brand = props.item.brand;
    this.title = props.item.title;
    this.description = props.item.description;
    this.descriptionFull = props.item.descriptionFull;
    this.price = props.item.price;
    this.currency = props.item.currency;
  }

  render() {
    const {
      brand,
      title,
      description,
      descriptionFull,
      price,
      currency
    } = this.props.item;

    return (
      <div className="main-content">
        <h2>{brand}</h2>
        <h1>{title}</h1>
        <h3>{description}</h3>
        <div className="description">{descriptionFull}</div>
        <div className="highlight-window mobile">
          <div className="highlight-overlay" />
        </div>
        <div className="divider" />
        <div className="purchase-info">
          <div className="price">
            {currency}
            {price}
          </div>
          <button>Добавить в корзину</button>
        </div>
      </div>
    );
  }
}

ItemClass.propTypes = {
  item: PropTypes.exact({
    brand: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    descriptionFull: PropTypes.string,
    price: PropTypes.number,
    currency: PropTypes.string,
  }).isRequired,
};
